Hello everyone,

I created 2 projects to demo what I have done. 

The first one is a react-ui and you can run it using this command:
docker-compose -f docker-compose.dev.yml up. You can see the result on http://localhost:3000. 
Also I have published it on this address which is one of my test domains : http://hiradmn.ir
I did not work on the ui and I just worked on the functionality

The second project has been written using C# and Windows Form technology and I could not dockerize it. 
You can go to the bin folder and run the exe file on windows operating system.

I had a lot of more options like WPF, WCF, WEB API, MVC CORE(including UI), NodeJS Based API and so on and 
I can change my stack easily.


I look forward to hearing from you.

Best regards,
Hiwa

