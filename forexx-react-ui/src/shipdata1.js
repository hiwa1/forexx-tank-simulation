export const data1 = [
  {
    "Name": "ENDEAVOUR",
    "YearBuilt": 2018,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 36779,
        "FillPercentage": 74
      },
      {
        "Product": "LNG",
        "Capacity": 19792,
        "FillPercentage": 69
      },
      {
        "Product": "LNG",
        "Capacity": 35947,
        "FillPercentage": 12
      },
      {
        "Product": "LNG",
        "Capacity": 36284,
        "FillPercentage": 35
      }
    ]
  },
  {
    "Name": "ENTERPRISE",
    "YearBuilt": 2017,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 20179,
        "FillPercentage": 59
      },
      {
        "Product": "LPG",
        "Capacity": 22467,
        "FillPercentage": 45
      },
      {
        "Product": "LNG",
        "Capacity": 42056,
        "FillPercentage": 33
      },
      {
        "Product": "LPG",
        "Capacity": 14032,
        "FillPercentage": 21
      }
    ]
  },
  {
    "Name": "RANGER",
    "YearBuilt": 2018,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 34146,
        "FillPercentage": 60
      },
      {
        "Product": "LNG",
        "Capacity": 25761,
        "FillPercentage": 70
      },
      {
        "Product": "LNG",
        "Capacity": 28341,
        "FillPercentage": 17
      }
    ]
  },
  {
    "Name": "FREEDOM",
    "YearBuilt": 2021,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 15179,
        "FillPercentage": 18
      },
      {
        "Product": "LNG",
        "Capacity": 14614,
        "FillPercentage": 94
      },
      {
        "Product": "LPG",
        "Capacity": 28067,
        "FillPercentage": 75
      },
      {
        "Product": "LNG",
        "Capacity": 23249,
        "FillPercentage": 97
      }
    ]
  },
  {
    "Name": "CONSTELLATION",
    "YearBuilt": 2019,
    "Tanks": [
      {
        "Product": "LPG",
        "Capacity": 24360,
        "FillPercentage": 66
      },
      {
        "Product": "LPG",
        "Capacity": 23578,
        "FillPercentage": 23
      }
    ]
  },
  {
    "Name": "COURAGEOUS",
    "YearBuilt": 2019,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 28116,
        "FillPercentage": 59
      },
      {
        "Product": "LPG",
        "Capacity": 13022,
        "FillPercentage": 19
      },
      {
        "Product": "LPG",
        "Capacity": 24019,
        "FillPercentage": 92
      },
      {
        "Product": "LNG",
        "Capacity": 43931,
        "FillPercentage": -1
      }
    ]
  },
  {
    "Name": "AURORA",
    "YearBuilt": 2020,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 42446,
        "FillPercentage": 94
      }
    ]
  },
  {
    "Name": "AMBER",
    "YearBuilt": 2020,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 21969,
        "FillPercentage": 44
      },
      {
        "Product": "LNG",
        "Capacity": 16633,
        "FillPercentage": 2
      },
      {
        "Product": "LNG",
        "Capacity": 23807,
        "FillPercentage": 82
      },
      {
        "Product": "LNG",
        "Capacity": 14896,
        "FillPercentage": 74
      },
      {
        "Product": "LNG",
        "Capacity": 15539,
        "FillPercentage": 26
      }
    ]
  },
  {
    "Name": "ARTEMIS",
    "YearBuilt": 2020,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 41610,
        "FillPercentage": 45
      },
      {
        "Product": "LNG",
        "Capacity": 15539,
        "FillPercentage": 26
      },
      {
        "Product": "LNG",
        "Capacity": 28307,
        "FillPercentage": -1
      },
      {
        "Product": "LNG",
        "Capacity": 43042,
        "FillPercentage": 76
      }
    ]
  },
  {
    "Name": "RESOLUTE",
    "YearBuilt": 2019,
    "Tanks": [
      {
        "Product": "LNG",
        "Capacity": 35324,
        "FillPercentage": 50
      },
      {
        "Product": "LNG",
        "Capacity": 18499,
        "FillPercentage": -1
      },
      {
        "Product": "LNG",
        "Capacity": 25126,
        "FillPercentage": 82
      },
      {
        "Product": "LNG",
        "Capacity": 32734,
        "FillPercentage": 69
      }
    ]
  }
];