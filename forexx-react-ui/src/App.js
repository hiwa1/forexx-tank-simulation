import React from 'react';
import Tabs from "./components/tabs/Tabs";
import "./App.css";
import { data1 } from "./shipdata1";
import Ship from "./components/ship/ship.component";
import { getAvailabe, getCapacity, getCopy } from './utils/common'

import  data from "./shipdata.json";

class App extends React.Component {

  constructor() {
    super()
    
    this.state = {
      year: 2019,
      percentage: 80,
      neededLiters: 0,
      cubicMeters: 150,
      numberOfNeededShips: 0,
      neededShips: data,
      sorted: data,
      ships: data
    };
  }

  onTextChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    this.setState({[name] : value})
    console.log(value)
    console.log(name)
  }

  calculateLiters = () => {
    var liters = 0;
    this.state.ships.forEach(p => {
      if (p.YearBuilt == this.state.year) {
        p.Tanks.forEach(q => {
          if (q.FillPercentage != -1)
            liters += (this.state.percentage - q.FillPercentage) * q.Capacity / 100
        })
      }
    })
    this.setState({ neededLiters: liters })
    console.log(this.state)

  }
  calculateNumberOfNeededShips = () => {

    let cubicMeters = this.state.cubicMeters * 1000;

    var sortedShips = this.state.ships.map(p =>
    ({
      ship: p,
      value: getAvailabe(p.Tanks)
    })).sort(function (a, b) { return b.value - a.value });
    console.log(sortedShips)
    var neededShips = [];

    for (let i = 0; i < sortedShips.length && cubicMeters > 0; i++) {
      cubicMeters -= sortedShips[i].value;
      neededShips.push(sortedShips[i].ship);
    }
    this.setState({ neededShips: neededShips, numberOfNeededShips: neededShips.length })


  }
  sortByCapacity = () => {

    let sortedShipsByCapacity= this.getSortedByCapacity(data)
    this.setState({ sorted: sortedShipsByCapacity })
  }
  sortByAvailable = () => {

    let sortedShipsByAvailable= this.getSortedByAvailable(data)
    this.setState({ sorted: sortedShipsByAvailable})
  }

  getSortedByAvailable = (arr)=>{
    arr.sort(function (a, b) { return getAvailabe(b.Tanks) - getAvailabe(a.Tanks) })
    return arr
  }

  getSortedByCapacity = (arr)=>{
    arr.sort(function (a, b) { return getCapacity(b.Tanks) - getCapacity(a.Tanks) })
    return arr
  }



  render() {
    return (
      <div>
        <h1>Forexx Tank Simulation</h1>
        <Tabs>
          <div label="Assignment #1">
            Year <input type="text" name='year' value={this.state.year} onChange={this.onTextChange}></input>
            Percentage <input type="text" name='percentage' value={this.state.percentage} onChange={this.onTextChange}></input>
            <button onClick={this.calculateLiters}>Calculate</button>
            <br /><br /><label>Needed liters:{this.state.neededLiters.toFixed()}</label>
            <hr />
            {
              this.state.ships.map(p => <Ship ship={p} />)
            }
          </div>
          <div label="Assignment #2">
            Cubic meters <input type="text" name='cubicMeters' value={this.state.cubicMeters} onChange={this.onTextChange}></input>
            <button onClick={this.calculateNumberOfNeededShips}>Calculate</button>
            <br /><br /><label>Number of needed ships:{this.state.numberOfNeededShips.toFixed()}</label>
            <hr />
            {
              this.state.neededShips.map(p => <Ship ship={p} />)
            }
          </div>
          <div label="Assignment #3">
            <button onClick={this.sortByCapacity}>Sort by capacity</button>
            <button onClick={this.sortByAvailable}>Sort by available capacity</button>
            <hr />
              <div label="Sorted ships">
                {
                  this.state.sorted.map(p => <Ship ship={p} />)
                }
              </div>
              
          </div>
        </Tabs>
      </div>
    );
  }
}

export default App;
