import React from 'react'
import Tank from '../tank/tank.component';
import './ship.styles.css'
import {getAvailabe,getCapacity} from '../../utils/common'


function Ship({ship}){

    return(
        <>
   <div className='ship'>
            <div>Name:{ship.Name}</div>
            <div>Year:{ship.YearBuilt}</div>
            <div>Capacity:{getCapacity(ship.Tanks).toFixed()}</div>
            <div>Available:{getAvailabe(ship.Tanks).toFixed()}</div>
            <div className='shipContainer' >
            {
                ship.Tanks.map(p=><Tank tank={p}/>)
            }
            </div>
            <hr/>
            </div>
        </>
    )
}

export default Ship;