
export function getAvailabe(tanks){
    let cap = 0
    tanks.forEach(q=>{
      if(q.FillPercentage!== -1){
        cap += ((100 - q.FillPercentage)* q.Capacity)/100
      }
    });
    return cap;
  }

  export function getCapacity (tanks) {
    let cap = 0
    tanks.forEach(q=>{
      if(q.FillPercentage!== -1){
        cap +=  q.Capacity 
      }
    });
    return cap;
  }

  export function getCopy(arr){
      return JSON.parse(JSON.stringify(arr)); 
  }

  