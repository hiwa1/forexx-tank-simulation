using Forexx.Models.Entities;
using System.Collections.Generic;
using Xunit;

namespace Forexx.UnitTest
{
    public class AssignmentTest
    {
        List<Ship> ships = new List<Ship>
            {
                new Ship
                {
                    Name =  "A",
                    YearBuilt = 2019,
                    Tanks = new List<Tank>
                    {
                        new Tank
                        {
                            Capacity =  1000,
                            FillPercentage = 32,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  2000,
                            FillPercentage = -1,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  500,
                            FillPercentage = 8,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  800,
                            FillPercentage = 60,
                            Product = "Pro"
                        },

                    }
                },
                new Ship
                {
                    Name =  "A",
                    YearBuilt = 2018,
                    Tanks = new List<Tank>
                    {
                        new Tank
                        {
                            Capacity =  5000,
                            FillPercentage = 40,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  12000,
                            FillPercentage = 8,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  7600,
                            FillPercentage = 44,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  230,
                            FillPercentage = 22,
                            Product = "Pro"
                        },

                    }
                },
                new Ship
                {
                    Name =  "A",
                    YearBuilt = 2019,
                    Tanks = new List<Tank>
                    {
                        new Tank
                        {
                            Capacity =  400,
                            FillPercentage = 17,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  300,
                            FillPercentage = 46,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  2500,
                            FillPercentage = 59,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  8000,
                            FillPercentage = 45,
                            Product = "Pro"
                        },

                    }
                },
            };
        [Fact]
        public void Litter_is_needed_Test()
        {
            
            var result = Assignments.CalculateLiters(ships, 2019, 50);
            Assert.Equal(629,result);
        }

        [Fact]
        public void Cubic_litters_ships_are_needed_Test()
        {
            List<Ship> selected = new List<Ship>
            {
                new Ship
                {
                    Name =  "A",
                    YearBuilt = 2018,
                    Tanks = new List<Tank>
                    {
                        new Tank
                        {
                            Capacity =  5000,
                            FillPercentage = 40,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  12000,
                            FillPercentage = 8,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  7600,
                            FillPercentage = 44,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  230,
                            FillPercentage = 22,
                            Product = "Pro"
                        },

                    }
                },
                new Ship
                {
                    Name =  "A",
                    YearBuilt = 2019,
                    Tanks = new List<Tank>
                    {
                        new Tank
                        {
                            Capacity =  400,
                            FillPercentage = 17,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  300,
                            FillPercentage = 46,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  2500,
                            FillPercentage = 59,
                            Product = "Pro"
                        },
                        new Tank
                        {
                            Capacity =  8000,
                            FillPercentage = 45,
                            Product = "Pro"
                        },

                    }
                },
            };
            var result = Assignments.CalculateCubicMeters(ships, 23);
            Assert.Equal(selected.Count, result.Count);

            Assert.Equal(selected[0].Tanks[0].Capacity, result[0].Tanks[0].Capacity);
            Assert.Equal(selected[selected.Count-1].Tanks[0].Capacity, result[result.Count-1].Tanks[0].Capacity);
        }

        [Fact]
        public void Sort_Test()
        {
            //var result = Assignments.Sort(null);
            //Assert.Equal(null, result);
        }
    }
}