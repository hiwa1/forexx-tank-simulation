﻿using Forexx.Models;
using Forexx.Models.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forexx.Models.GeneralClasses
{
    public class Utility
    {
        public static List<Ship> ReadJsonFile(string address)
        {
            string data = File.ReadAllText(address);
            return JsonConvert.DeserializeObject<List<Ship>>(data);
        }
    }
}
