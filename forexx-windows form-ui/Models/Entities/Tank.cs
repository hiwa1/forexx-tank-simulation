﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forexx.Models.Entities
{
    public class Tank
    {
        public string Product { get; set; }
        public int Capacity { get; set; }
        public int FillPercentage { get; set; }
    }
}
