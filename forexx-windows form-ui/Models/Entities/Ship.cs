﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forexx.Models.Entities
{
    public class Ship
    {
        public string Name { get; set; }
        public int YearBuilt { get; set; }
        public List<Tank> Tanks { get; set; }
    }
}
