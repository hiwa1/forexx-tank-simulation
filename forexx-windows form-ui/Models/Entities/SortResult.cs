﻿using Forexx.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entities
{
    public class SortResult
    {
        public List<Ship> SortByTotal{ get; set; }
        public List<Ship> SortByAvailable { get; set; }
    }
}
