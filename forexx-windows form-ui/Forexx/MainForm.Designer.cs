﻿namespace Forexx
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnlLiters = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLitersResult = new System.Windows.Forms.Label();
            this.btnCalculateLiters = new System.Windows.Forms.Button();
            this.txtPercentage = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnlCubic = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumberOfNeededShips = new System.Windows.Forms.Label();
            this.btnCalculateCubicMeters = new System.Windows.Forms.Button();
            this.txtCubicMeters = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pnlCapacity = new System.Windows.Forms.Panel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.pnlAvailable = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSort = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1302, 621);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlLiters);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1294, 595);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Assignment #1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnlLiters
            // 
            this.pnlLiters.AutoScroll = true;
            this.pnlLiters.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlLiters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLiters.Location = new System.Drawing.Point(3, 53);
            this.pnlLiters.Name = "pnlLiters";
            this.pnlLiters.Size = new System.Drawing.Size(1288, 539);
            this.pnlLiters.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblLitersResult);
            this.panel1.Controls.Add(this.btnCalculateLiters);
            this.panel1.Controls.Add(this.txtPercentage);
            this.panel1.Controls.Add(this.txtYear);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1288, 50);
            this.panel1.TabIndex = 0;
            // 
            // lblLitersResult
            // 
            this.lblLitersResult.AutoSize = true;
            this.lblLitersResult.Location = new System.Drawing.Point(605, 18);
            this.lblLitersResult.Name = "lblLitersResult";
            this.lblLitersResult.Size = new System.Drawing.Size(37, 13);
            this.lblLitersResult.TabIndex = 5;
            this.lblLitersResult.Text = "Result";
            // 
            // btnCalculateLiters
            // 
            this.btnCalculateLiters.Location = new System.Drawing.Point(375, 13);
            this.btnCalculateLiters.Name = "btnCalculateLiters";
            this.btnCalculateLiters.Size = new System.Drawing.Size(75, 23);
            this.btnCalculateLiters.TabIndex = 4;
            this.btnCalculateLiters.Text = "Calculate";
            this.btnCalculateLiters.UseVisualStyleBackColor = true;
            this.btnCalculateLiters.Click += new System.EventHandler(this.btnCalculateLiters_Click);
            // 
            // txtPercentage
            // 
            this.txtPercentage.Location = new System.Drawing.Point(226, 15);
            this.txtPercentage.Name = "txtPercentage";
            this.txtPercentage.Size = new System.Drawing.Size(100, 20);
            this.txtPercentage.TabIndex = 3;
            this.txtPercentage.Text = "80";
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(41, 15);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(100, 20);
            this.txtYear.TabIndex = 2;
            this.txtYear.Text = "2019";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Percentage";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Year";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnlCubic);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1294, 595);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Assignment #2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnlCubic
            // 
            this.pnlCubic.AutoScroll = true;
            this.pnlCubic.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlCubic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCubic.Location = new System.Drawing.Point(3, 53);
            this.pnlCubic.Name = "pnlCubic";
            this.pnlCubic.Size = new System.Drawing.Size(1288, 539);
            this.pnlCubic.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblNumberOfNeededShips);
            this.panel2.Controls.Add(this.btnCalculateCubicMeters);
            this.panel2.Controls.Add(this.txtCubicMeters);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1288, 50);
            this.panel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(464, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Number of needed ships";
            // 
            // lblNumberOfNeededShips
            // 
            this.lblNumberOfNeededShips.AutoSize = true;
            this.lblNumberOfNeededShips.Location = new System.Drawing.Point(623, 16);
            this.lblNumberOfNeededShips.Name = "lblNumberOfNeededShips";
            this.lblNumberOfNeededShips.Size = new System.Drawing.Size(37, 13);
            this.lblNumberOfNeededShips.TabIndex = 6;
            this.lblNumberOfNeededShips.Text = "Result";
            // 
            // btnCalculateCubicMeters
            // 
            this.btnCalculateCubicMeters.Location = new System.Drawing.Point(280, 10);
            this.btnCalculateCubicMeters.Name = "btnCalculateCubicMeters";
            this.btnCalculateCubicMeters.Size = new System.Drawing.Size(75, 23);
            this.btnCalculateCubicMeters.TabIndex = 4;
            this.btnCalculateCubicMeters.Text = "Calculate";
            this.btnCalculateCubicMeters.UseVisualStyleBackColor = true;
            this.btnCalculateCubicMeters.Click += new System.EventHandler(this.btnCalculateCubicMeters_Click);
            // 
            // txtCubicMeters
            // 
            this.txtCubicMeters.Location = new System.Drawing.Point(148, 13);
            this.txtCubicMeters.Name = "txtCubicMeters";
            this.txtCubicMeters.Size = new System.Drawing.Size(100, 20);
            this.txtCubicMeters.TabIndex = 2;
            this.txtCubicMeters.Text = "150";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Amount of cubic meters";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tabControl2);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1294, 595);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Assignment #3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 47);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1294, 548);
            this.tabControl2.TabIndex = 3;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pnlCapacity);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1286, 522);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Sorted by capacity";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pnlCapacity
            // 
            this.pnlCapacity.AutoScroll = true;
            this.pnlCapacity.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlCapacity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCapacity.Location = new System.Drawing.Point(3, 3);
            this.pnlCapacity.Name = "pnlCapacity";
            this.pnlCapacity.Size = new System.Drawing.Size(1280, 516);
            this.pnlCapacity.TabIndex = 2;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.pnlAvailable);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1286, 522);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Sorted by available capacity";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // pnlAvailable
            // 
            this.pnlAvailable.AutoScroll = true;
            this.pnlAvailable.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlAvailable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAvailable.Location = new System.Drawing.Point(3, 3);
            this.pnlAvailable.Name = "pnlAvailable";
            this.pnlAvailable.Size = new System.Drawing.Size(1280, 516);
            this.pnlAvailable.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.btnSort);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1294, 47);
            this.panel3.TabIndex = 2;
            // 
            // btnSort
            // 
            this.btnSort.Location = new System.Drawing.Point(8, 14);
            this.btnSort.Name = "btnSort";
            this.btnSort.Size = new System.Drawing.Size(75, 23);
            this.btnSort.TabIndex = 4;
            this.btnSort.Text = "Sort";
            this.btnSort.UseVisualStyleBackColor = true;
            this.btnSort.Click += new System.EventHandler(this.btnSort_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(505, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Needed liters";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1302, 621);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ship and Tank Simulation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCalculateLiters;
        private System.Windows.Forms.TextBox txtPercentage;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCalculateCubicMeters;
        private System.Windows.Forms.TextBox txtCubicMeters;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSort;
        private System.Windows.Forms.Label lblLitersResult;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label lblNumberOfNeededShips;
        private System.Windows.Forms.Panel pnlLiters;
        private System.Windows.Forms.Panel pnlCubic;
        private System.Windows.Forms.Panel pnlCapacity;
        private System.Windows.Forms.Panel pnlAvailable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}

