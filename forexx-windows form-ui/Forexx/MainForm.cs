﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Forexx.Components;
using Forexx.Models.Entities;
using Forexx.Models.GeneralClasses;
using Newtonsoft.Json;

namespace Forexx
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();            
            CheckForIllegalCrossThreadCalls = false;
            CalculateLiters();
            CalculateCubicMeters();
            Sort();
        }

        

        private void CalculateLiters()
        {
            try
            {
                var ships = Utility.ReadJsonFile("shipdata.json");
                int year = int.Parse(txtYear.Text);
                int percentage = int.Parse(txtPercentage.Text);
                var liters = Assignments.CalculateLiters(ships, year, percentage);

                lblLitersResult.Text = liters.ToString();
                ships.ForEach(ship =>
                {
                    ShipControl com = new ShipControl(ship);
                    com.Dock = DockStyle.Top;
                    pnlLiters.Controls.Add(com);
                });
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
            
        }

        private void CalculateCubicMeters()
        {
            try
            {
                var ships = Utility.ReadJsonFile("shipdata.json");
                var cubicMeters = int.Parse(txtCubicMeters.Text);
                var neededShips = Assignments.CalculateCubicMeters(ships, cubicMeters);

                lblNumberOfNeededShips.Text = $"{neededShips.Count.ToString()} Ships";

                neededShips.ForEach(ship =>
                {
                    ShipControl com = new ShipControl(ship);
                    com.Dock = DockStyle.Top;
                    pnlCubic.Controls.Add(com);
                });
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
            
        }

        private void Sort()
        {
            try
            {
                var ships = Utility.ReadJsonFile("shipdata.json");
                var sortedLists = Assignments.Sort(ships);


                sortedLists.SortByTotal.ForEach(ship =>
                {
                    ShipControl com = new ShipControl(ship);
                    com.Dock = DockStyle.Top;
                    pnlCapacity.Controls.Add(com);
                });

                sortedLists.SortByAvailable.ForEach(ship =>
                {
                    ShipControl com = new ShipControl(ship);
                    com.Dock = DockStyle.Top;
                    pnlAvailable.Controls.Add(com);
                });
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
            
        }

        private void btnCalculateLiters_Click(object sender, EventArgs e)
        {
            CalculateLiters();
        }

        private void btnCalculateCubicMeters_Click(object sender, EventArgs e)
        {
            CalculateCubicMeters();
        }

        

        private void btnSort_Click(object sender, EventArgs e)
        {
            Sort();
        }

        
    }
}
