﻿namespace Forexx.Components
{
    partial class ShipControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblYear = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblShipName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTanks = new System.Windows.Forms.Panel();
            this.lblMax = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAvailable = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.lblAvailable);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblMax);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblYear);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblShipName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1315, 41);
            this.panel1.TabIndex = 0;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblYear.Location = new System.Drawing.Point(230, 15);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(29, 13);
            this.lblYear.TabIndex = 3;
            this.lblYear.Text = "Year";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Year";
            // 
            // lblShipName
            // 
            this.lblShipName.AutoSize = true;
            this.lblShipName.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblShipName.Location = new System.Drawing.Point(79, 15);
            this.lblShipName.Name = "lblShipName";
            this.lblShipName.Size = new System.Drawing.Size(57, 13);
            this.lblShipName.TabIndex = 1;
            this.lblShipName.Text = "Ship name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ship name";
            // 
            // pnlTanks
            // 
            this.pnlTanks.AutoScroll = true;
            this.pnlTanks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTanks.Location = new System.Drawing.Point(0, 41);
            this.pnlTanks.Name = "pnlTanks";
            this.pnlTanks.Size = new System.Drawing.Size(1315, 180);
            this.pnlTanks.TabIndex = 1;
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblMax.Location = new System.Drawing.Point(420, 15);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(26, 13);
            this.lblMax.TabIndex = 5;
            this.lblMax.Text = "max";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(309, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Maximum capacity";
            // 
            // lblAvailable
            // 
            this.lblAvailable.AutoSize = true;
            this.lblAvailable.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblAvailable.Location = new System.Drawing.Point(602, 15);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(49, 13);
            this.lblAvailable.TabIndex = 7;
            this.lblAvailable.Text = "available";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(491, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Available capacity";
            // 
            // ShipControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.pnlTanks);
            this.Controls.Add(this.panel1);
            this.Name = "ShipControl";
            this.Size = new System.Drawing.Size(1315, 221);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblShipName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlTanks;
        private System.Windows.Forms.Label lblAvailable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label label4;
    }
}
