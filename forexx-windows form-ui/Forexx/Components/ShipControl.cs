﻿using Forexx.Models.Entities;
using Forexx.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XLControls;

namespace Forexx.Components
{
    public partial class ShipControl : UserControl
    {
        public ShipControl()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }
        public ShipControl(Ship ship)
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            lblShipName.Text = ship.Name;
            lblYear.Text = ship.YearBuilt.ToString();


            lblMax.Text = ship.Tanks.Where(p => p.FillPercentage != -1).Sum(p => p.Capacity).ToString();
            lblAvailable.Text = ship.Tanks.Where(p => p.FillPercentage != -1).Sum(p =>(100 - p.FillPercentage) * p.Capacity/100).ToString();

            ship.Tanks.ForEach(p =>
            {
                LiquidContainerControl tank = GetTank(p);
                this.pnlTanks.Controls.Add(tank);
                tank.Refresh();
            });
        }

        private static XLControls.LiquidContainerControl GetTank(Tank t)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            var tank = new XLControls.LiquidContainerControl(t.Capacity);
            tank.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            tank.ConstantText = "Level";
            tank.CriticalLevelColor = System.Drawing.Color.Red;
            tank.CriticalLevelColor2 = System.Drawing.Color.MistyRose;
            tank.CriticalLevelValue = 15;
            tank.DisplayConstantText = true;
            tank.DisplayValueAsPercentage = true;
            tank.DisplayValueText = true;
            tank.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tank.Image = null;
            tank.ImageFocused = null;
            tank.ImageIdle = ((System.Drawing.Image)Image.FromFile("bitmapButton2.ImageIdle.png"));
                //(resources.GetObject("bitmapButton2.ImageIdle"))); ; ; ; ; ;
            tank.ImageInactive = null;
            tank.ImageMouseOver = null;
            tank.ImagePressed = null;
            tank.InactiveColor = System.Drawing.Color.Silver;
            //bitmapButton2.Location = new System.Drawing.Point(288, 57);
            tank.Name = "bitmapButton2";
            tank.NormalLevelColor = System.Drawing.Color.DodgerBlue;
            tank.NormalLevelColor2 = System.Drawing.Color.LightCyan;
            tank.OffsetPressedContent = true;
            tank.Size = new System.Drawing.Size(289, 328);
            tank.StretchImage = true;
            tank.TabIndex = 1;
            tank.Text = "bitmapButton1";
            tank.TextDropShadow = true;
            tank.UseVisualStyleBackColor = false;
            tank.Value = 0;
            tank.WarningLevelColor = System.Drawing.Color.Orange;
            tank.WarningLevelColor2 = System.Drawing.Color.OldLace;
            tank.WarningLevelValue = 30;
            tank.Value = t.FillPercentage;
            tank.Dock = DockStyle.Left;
            tank.Margin = new System.Windows.Forms.Padding(10);
            tank.Refresh();
            return tank;
        }
    }
}
