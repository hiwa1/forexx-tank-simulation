﻿using Forexx.Models.Entities;
using Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forexx
{
    public class Assignments
    {
        public static int CalculateLiters(List<Ship> ships, int year, int percentage)
        {

            var liters = ships.Where(p => p.YearBuilt == year).
                Sum(x => x.Tanks.Where(p => p.FillPercentage != -1).Sum(q => (percentage - q.FillPercentage) * q.Capacity / 100));
            return liters;
        }


        public static List<Ship> CalculateCubicMeters(List<Ship> ships, int cubicMeters)
        {

            cubicMeters *= 1000;

            var sortedShips = ships.Select(p => new
            {
                Ship = p,
                Value = p.Tanks.Where(q => q.FillPercentage != -1).
                Sum(q => (100 - q.FillPercentage) * q.Capacity / 100)
            }).OrderByDescending(p => p.Value).ToList();



            //another solution
            //var sum = 0;
            //var neededShips = sortedShips.TakeWhile(x => (sum += x.Value) < totalLitters).Select(p=>p.Ship).ToList();

            var neededShips = new List<Ship>();

            for (int i = 0; i < sortedShips.Count && cubicMeters > 0; i++)
            {
                cubicMeters -= sortedShips[i].Value;
                neededShips.Add(sortedShips[i].Ship);
            }

            return neededShips;
            
        }


        public static SortResult Sort(List<Ship> ships)
        {
            var sortedShipsTotal = ships.OrderBy(p => p.Tanks.Where(q => q.FillPercentage != -1).
            Sum(q => q.Capacity)).ToList();
            var sortedShipsRemainingCapacity = ships.OrderBy(p => p.Tanks.Where(q => q.FillPercentage != -1).
            Sum(q => (100 - q.FillPercentage) * q.Capacity / 100)).ToList();

            return new SortResult
            {
                SortByTotal = sortedShipsTotal,
                SortByAvailable = sortedShipsRemainingCapacity
            };
        }
    }
}
