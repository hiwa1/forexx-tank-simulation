using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace XLControls
{
    /// <summary>
    /// possible button states
    /// </summary>
    public enum ButtonState
    {
        Disabled = 0,
        Idle = 1,
        MouseOver = 2,
        Pushed = 3,
    }

    /// <summary>
    /// This class displays a liquid container. We use it for displaying Ink Tanks.
    /// </summary>
    public class LiquidContainerControl : Button
    {
        #region Privates

        private Image imageNormal = null;
        private Image imageFocused = null;
        private Image imagePressed = null;
        private Image imageMouseOver = null;
        private Image imageInactive = null;

        private Color normalLevelColor = Color.DodgerBlue;
        private Color warningLevelColor = Color.Orange;
        private Color criticalLevelColor = Color.Red;
        private Color normalLevelColor2 = Color.DodgerBlue;
        private Color warningLevelColor2 = Color.Orange;
        private Color criticalLevelColor2 = Color.Red;

        private Color inactiveColor = Color.LightGray;
        private bool stretchImage = false;
        private bool textDropShadow = true;
        private int padding = 5;
        private int liquidValue = 0;
        private int criticalLevelValue = 15;
        private int warningLevelValue = 30;
        private bool offsetPressedContent = true;
        private ButtonState btnState = ButtonState.Idle;
        private bool capturingMouse = false;
        private bool displayConstantText = false;
        private string constantText = string.Empty;
        private bool displayValueText = true;
        private bool displayValueAsPercentage = false;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the image that is displayed on a button control.
        /// </summary>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        ///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
        ///   <IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        /// </PermissionSet>
        [Browsable(false)]
        new public Image Image
        {
            get { return base.Image; }
            set { base.Image = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="T:System.Windows.Forms.ImageList" /> that contains the <see cref="T:System.Drawing.Image" /> displayed on a button control.
        /// </summary>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        /// </PermissionSet>
        [Browsable(false)]
        new public ImageList ImageList
        {
            get { return base.ImageList; }
            set { base.ImageList = value; }
        }

        /// <summary>
        /// Gets or sets the image list index value of the image displayed on the button control.
        /// </summary>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        /// </PermissionSet>
        [Browsable(false)]
        new public int ImageIndex
        {
            get { return base.ImageIndex; }
            set { base.ImageIndex = value; }
        }

        /// <summary>
        /// Enable the shadowing of the button text
        /// </summary>
        /// <value>
        ///   <c>true</c> if [text drop shadow]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("enables the text to cast a shadow"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool TextDropShadow
        {
            get { return textDropShadow; }
            set { textDropShadow = value; }
        }


        /// <summary>
        /// Gets or sets the color of the critical level.
        /// </summary>
        /// <value>
        /// The color of the critical level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color of the border around the image"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color CriticalLevelColor
        {
            get { return criticalLevelColor; }
            set { criticalLevelColor = value; }
        }

        /// <summary>
        /// Gets or sets the color 2 of the critical level.
        /// </summary>
        /// <value>
        /// The color 2 of the critical level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color 2 of the border around the image"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color CriticalLevelColor2
        {
            get { return criticalLevelColor2; }
            set { criticalLevelColor2 = value; }
        }

        /// <summary>
        /// Gets or sets the color of the inactive.
        /// </summary>
        /// <value>
        /// The color of the inactive.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color of the border around the image"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color InactiveColor
        {
            get { return inactiveColor; }
            set { inactiveColor = value; }
        }

        /// <summary>
        /// Gets or sets the color of the warning level.
        /// </summary>
        /// <value>
        /// The color of the warning level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color of liquid when level is Warning"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color WarningLevelColor
        {
            get { return warningLevelColor; }
            set { warningLevelColor = value; }
        }

        /// <summary>
        /// Gets or sets the color 2 of the warning level.
        /// </summary>
        /// <value>
        /// The color of the warning level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color 2 of liquid when level is Warning"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color WarningLevelColor2
        {
            get { return warningLevelColor2; }
            set { warningLevelColor2 = value; }
        }

        /// <summary>
        /// Gets or sets the color of the normal level.
        /// </summary>
        /// <value>
        /// The color of the normal level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color of liquid when level is Idle"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color NormalLevelColor
        {
            get { return normalLevelColor; }
            set { normalLevelColor = value; }
        }

        /// <summary>
        /// Gets or sets the color of the normal level.
        /// </summary>
        /// <value>
        /// The color of the normal level.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Color 2 of liquid when level is Idle"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Color NormalLevelColor2
        {
            get { return normalLevelColor2; }
            set { normalLevelColor2 = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [stretch image].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stretch image]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("stretch the impage to the size of the button"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool StretchImage
        {
            get { return stretchImage; }
            set { stretchImage = value; }
        }


        /// <summary>
        /// Gets or sets padding within the control.
        /// </summary>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
        /// </PermissionSet>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public new int Padding
        {
            get { return padding; }
            set { padding = value; }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("amount of liquid to display"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public int Value
        {
            get { return liquidValue; }
            set { liquidValue = value; }
        }

        /// <summary>
        /// Gets or sets the critical level value.
        /// </summary>
        /// <value>
        /// The critical level value.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("critical value to change liquid color"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public int CriticalLevelValue
        {
            get { return criticalLevelValue; }
            set { criticalLevelValue = value; }
        }

        /// <summary>
        /// Gets or sets the warning level value.
        /// </summary>
        /// <value>
        /// The warning level value.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("warning value to change liquid color"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public int WarningLevelValue
        {
            get { return warningLevelValue; }
            set { warningLevelValue = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [display constant text].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [display constant text]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("the constant text to display"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool DisplayConstantText
        {
            get { return displayConstantText; }
            set { displayConstantText = value; }
        }

        /// <summary>
        /// Gets or sets the constant text.
        /// </summary>
        /// <value>
        /// The constant text.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("should display constant text?"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public string ConstantText
        {
            get { return constantText; }
            set { constantText = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [display value text].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [display value text]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("should display the liquid Value text?"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool DisplayValueText
        {
            get { return displayValueText; }
            set { displayValueText = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [display value as percentage].
        /// </summary>
        /// <value>
        /// <c>true</c> if [display value as percentage]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("display liquid Value text as percentage?"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool DisplayValueAsPercentage
        {
            get { return displayValueAsPercentage; }
            set { displayValueAsPercentage = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether [offset pressed content].
        /// </summary>
        /// <value>
        /// <c>true</c> if [offset pressed content]; otherwise, <c>false</c>.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Set to true if to offset image/text when button is pressed (Push Effect On Image)"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public bool OffsetPressedContent
        {
            get
            {
                return
                    offsetPressedContent;
            }
            set { offsetPressedContent = value; }
        }


        /// <summary>
        /// Gets or sets the image normal.
        /// </summary>
        /// <value>
        /// The image normal.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Image to be displayed while the button state is in idle state"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Image ImageIdle
        {
            get { return imageNormal; }
            set { imageNormal = value; }
        }


        /// <summary>
        /// Gets or sets the image focused.
        /// </summary>
        /// <value>
        /// The image focused.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Image to be displayed while the button has focus"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Image ImageFocused
        {
            get { return imageFocused; }
            set { imageFocused = value; }
        }


        /// <summary>
        /// Gets or sets the image inactive.
        /// </summary>
        /// <value>
        /// The image inactive.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Image to be displayed while the button is inactive"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Image ImageInactive
        {
            get { return imageInactive; }
            set { imageInactive = value; }
        }


        /// <summary>
        /// Gets or sets the image pressed.
        /// </summary>
        /// <value>
        /// The image pressed.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Image to be displayed while the button state is pressed"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Image ImagePressed
        {
            get { return imagePressed; }
            set { imagePressed = value; }
        }


        /// <summary>
        /// Gets or sets the image mouse over.
        /// </summary>
        /// <value>
        /// The image mouse over.
        /// </value>
        [Browsable(true),
        CategoryAttribute("Appearance"),
        Description("Image to be displayed while the button state is MouseOver"),
        System.ComponentModel.RefreshProperties(RefreshProperties.Repaint)
        ]
        public Image ImageMouseOver
        {
            get { return imageMouseOver; }
            set { imageMouseOver = value; }
        }

        #endregion

        /// <summary>
        /// The components
        /// </summary>
        private System.ComponentModel.Container components = null;
        private readonly int capacity;


        /// <summary>
        /// Initializes a new instance of the <see cref="LiquidContainerControl"/> class.
        /// </summary>
        public LiquidContainerControl(int capacity)
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            this.capacity = capacity;
            // TODO: Add any initialization after the InitComponent call			
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.ButtonBase" /> and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }
        #endregion

        #region Paint Methods

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Paint" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            CreateRegion(0);
            PaintBackground(e);
            Paintimage(e);
            PaintText(e);
        }

        /// <summary>
        /// Paints the background.
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
        private void PaintBackground(PaintEventArgs e)
        {
            if (e == null)
                return;
            if (e.Graphics == null)
                return;

            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(0, 0, Size.Width, Size.Height);

            Color color = this.BackColor; ;
            if (btnState == ButtonState.Disabled)
                color = inactiveColor;

            System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(rect, this.BackColor, this.BackColor, System.Drawing.Drawing2D.LinearGradientMode.Vertical);
            g.FillRectangle(brush, rect);
            brush.Dispose();
        }


        /// <summary>
        /// Paints the text.
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
        private void PaintText(PaintEventArgs e)
        {
            if (e == null)
                return;
            if (e.Graphics == null)
                return;

            var font = this.Font;
            font = new Font(font.FontFamily, font.Size / 2);
            Rectangle rect = new Rectangle(0, 0, Size.Width, Size.Height);				//GetTextDestinationRect();

            if ((btnState == ButtonState.Pushed) && (OffsetPressedContent))
                rect.Offset(1, 1);

            string text = this.Text;
            string capacityText = this.capacity.ToString();

            if (displayConstantText)
            {
                text = constantText;

                if (displayValueText)
                {
                    if (displayValueAsPercentage)
                    {
                        text = string.Format("{0} {1}%", constantText, liquidValue);
                    }
                    else
                    {
                        text = string.Format("{0} {1}", constantText, liquidValue);
                    }
                }

            }
            else if (displayValueText)
            {
                if (displayValueAsPercentage)
                {
                    text = string.Format("{0}%", liquidValue);
                }
                else
                {
                    text = string.Format("{0}", liquidValue);
                }
            }

            capacityText = string.Format("{0} Litter",  capacityText);


            // Calc Rectagle for the text
            SizeF textSize = GetTextSize(e.Graphics, text, this.Font);
            SizeF capacityTextSize = GetTextSize(e.Graphics, capacityText, this.Font);
            capacityTextSize = new SizeF(capacityTextSize.Width / 2, capacityTextSize.Height / 2);



            // Calc starting location to point of the text
            Point textStartPoint = CalculateLeftEdgeTopEdge(this.TextAlign, rect, (int)textSize.Width, (int)textSize.Height);
            Point capacityTextStartPoint = CalculateLeftEdgeTopEdge(this.TextAlign, rect, (int)textSize.Width, (int)textSize.Height);

            capacityTextStartPoint = new Point(capacityTextStartPoint.X, capacityTextStartPoint.Y - 40);


            if (btnState == ButtonState.Disabled)
            {
                e.Graphics.DrawString(text , this.Font, new SolidBrush(Color.White), textStartPoint.X + 1, textStartPoint.Y + 1);
                e.Graphics.DrawString(text , this.Font, new SolidBrush(Color.FromArgb(50, 50, 50)), textStartPoint.X, textStartPoint.Y);
                e.Graphics.DrawString(text, this.Font, new SolidBrush(Color.White), capacityTextStartPoint.X + 1, capacityTextStartPoint.Y + 1);
                e.Graphics.DrawString(text, this.Font, new SolidBrush(Color.FromArgb(50, 50, 50)), capacityTextStartPoint.X, capacityTextStartPoint.Y);
            }
            else
            {
                RectangleF rectangle;
                rect.Inflate((-15 * rect.Width) / 100, (-14 * rect.Height) / 100);

                if (TextDropShadow)
                {
                    Brush TransparentBrush0 = new SolidBrush(Color.FromArgb(50, Color.Black));
                    Brush TransparentBrush1 = new SolidBrush(Color.FromArgb(20, Color.Black));

                    rectangle = new RectangleF(textStartPoint.X, textStartPoint.Y + 1, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, this.Font, TransparentBrush0, rectangle);

                    rectangle = new RectangleF(textStartPoint.X + 1, textStartPoint.Y, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, this.Font, TransparentBrush0, rectangle);

                    rectangle = new RectangleF(textStartPoint.X + 1, textStartPoint.Y + 1, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, this.Font, TransparentBrush1, rectangle);

                    rectangle = new RectangleF(textStartPoint.X, textStartPoint.Y + 2, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, this.Font, TransparentBrush1, rectangle);

                    rectangle = new RectangleF(textStartPoint.X + 2, textStartPoint.Y, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, this.Font, TransparentBrush1, rectangle);


                    

                    rectangle = new RectangleF(capacityTextStartPoint.X, capacityTextStartPoint.Y + 1, rect.Width, rect.Height);
                    e.Graphics.DrawString(capacityText, font, TransparentBrush0, rectangle);

                    rectangle = new RectangleF(capacityTextStartPoint.X + 1, capacityTextStartPoint.Y, rect.Width, rect.Height);
                    e.Graphics.DrawString(capacityText, font, TransparentBrush0, rectangle);

                    rectangle = new RectangleF(capacityTextStartPoint.X + 1, capacityTextStartPoint.Y + 1, rect.Width, rect.Height);
                    e.Graphics.DrawString(capacityText, font, TransparentBrush1, rectangle);

                    rectangle = new RectangleF(capacityTextStartPoint.X, capacityTextStartPoint.Y + 2, rect.Width, rect.Height);
                    e.Graphics.DrawString(capacityText, font, TransparentBrush1, rectangle);

                    rectangle = new RectangleF(capacityTextStartPoint.X + 2, capacityTextStartPoint.Y, rect.Width, rect.Height);
                    e.Graphics.DrawString(capacityText, font, TransparentBrush1, rectangle);

                    TransparentBrush0.Dispose();
                    TransparentBrush1.Dispose();
                }

                rectangle = new RectangleF(textStartPoint.X, textStartPoint.Y, rect.Width, rect.Height);
                e.Graphics.DrawString(text, this.Font, new SolidBrush(this.ForeColor), rectangle);

                rectangle = new RectangleF(capacityTextStartPoint.X, capacityTextStartPoint.Y, rect.Width, rect.Height);
                e.Graphics.DrawString(capacityText, font, new SolidBrush(this.ForeColor), rectangle);
            }
        }

        /// <summary>
        /// Paints the image fill rect.
        /// </summary>
        /// <param name="g">The g.</param>
        /// <param name="ImageRect">The image rect.</param>
         private void PaintImageFillRect(Graphics g, Rectangle ImageRect)
        {
            Rectangle liquidRect = ImageRect;

            // Calc the rectangle
            liquidRect.Inflate((-15 * liquidRect.Width) / 100, (-14 * liquidRect.Height) / 100);
            liquidRect.Height = (Value * liquidRect.Height) / 100;

            Color selectedColor  = liquidRect.Height <= (warningLevelValue * ImageRect.Height / 100) && liquidRect.Height > (criticalLevelValue * ImageRect.Height / 100) ? warningLevelColor : liquidRect.Height <= (criticalLevelValue * ImageRect.Height / 100) ? criticalLevelColor : normalLevelColor;
            Color selectedColor2 = liquidRect.Height <= (warningLevelValue * ImageRect.Height / 100) && liquidRect.Height > (criticalLevelValue * ImageRect.Height / 100) ? warningLevelColor2 : liquidRect.Height <= (criticalLevelValue * ImageRect.Height / 100) ? criticalLevelColor2 : normalLevelColor2;

            if (btnState == ButtonState.Disabled) selectedColor = inactiveColor;
            System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(ImageRect, selectedColor, selectedColor2, 270, true);


             //Fill Rectabgle
             g.FillRectangle(brush, ((14 * ImageRect.Width) / 100), ImageRect.Height - ((14 * ImageRect.Height) / 100) - liquidRect.Height, liquidRect.Width, liquidRect.Height);
        }


         /// <summary>
         /// Paintimages the specified e.
         /// </summary>
         /// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
        private void Paintimage(PaintEventArgs e)
        {

            if (e == null)
                return;
            if (e.Graphics == null)
                return;

            Image image = GetCurrentImage(btnState);

            if (image != null)
            {
                Graphics g = e.Graphics;
                Rectangle rect = GetImageDestinationRect();

                if ((btnState == ButtonState.Pushed) && (offsetPressedContent))
                    rect.Offset(1, 1);

                if (this.StretchImage)
                {
                    g.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                }
                else
                {
                    Rectangle r = GetImageDestinationRect();
                    g.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                }

                PaintImageFillRect(g, rect);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets the size of the text.
        /// </summary>
        /// <param name="g">The g.</param>
        /// <param name="strText">The string text.</param>
        /// <param name="font">The font.</param>
        /// <returns></returns>
        private SizeF GetTextSize(Graphics g, string strText, Font font)
        {
            SizeF size = g.MeasureString(strText, font);
            return (size);
        }

        /// <summary>
        /// Gets the text destination rect.
        /// </summary>
        /// <returns></returns>
        private Rectangle GetTextDestinationRect()
        {
            Rectangle ImageRect = GetImageDestinationRect();
            Rectangle rect = new Rectangle(0, 0, 0, 0);
            switch (this.ImageAlign)
            {
                case ContentAlignment.BottomCenter:
                    rect = new Rectangle(0, 0, this.Width, ImageRect.Top);
                    break;
                case ContentAlignment.BottomLeft:
                    rect = new Rectangle(0, 0, this.Width, ImageRect.Top);
                    break;
                case ContentAlignment.BottomRight:
                    rect = new Rectangle(0, 0, this.Width, ImageRect.Top);
                    break;
                case ContentAlignment.MiddleCenter:
                    rect = new Rectangle(0, ImageRect.Bottom, this.Width, this.Height - ImageRect.Bottom);
                    break;
                case ContentAlignment.MiddleLeft:
                    rect = new Rectangle(ImageRect.Right, 0, this.Width - ImageRect.Right, this.Height);
                    break;
                case ContentAlignment.MiddleRight:
                    rect = new Rectangle(0, 0, ImageRect.Left, this.Height);
                    break;
                case ContentAlignment.TopCenter:
                    rect = new Rectangle(0, ImageRect.Bottom, this.Width, this.Height - ImageRect.Bottom);
                    break;
                case ContentAlignment.TopLeft:
                    rect = new Rectangle(0, ImageRect.Bottom, this.Width, this.Height - ImageRect.Bottom);
                    break;
                case ContentAlignment.TopRight:
                    rect = new Rectangle(0, ImageRect.Bottom, this.Width, this.Height - ImageRect.Bottom);
                    break;
            }
            rect.Inflate(-this.Padding, -this.Padding);
            return (rect);
        }

        /// <summary>
        /// Gets the image destination rect.
        /// </summary>
        /// <returns></returns>
        private Rectangle GetImageDestinationRect()
        {
            Rectangle rect = new Rectangle(0, 0, 0, 0);
            Image image = GetCurrentImage(this.btnState);
            if (image != null)
            {
                if (this.StretchImage)
                {
                    rect.Width = this.Width;
                    rect.Height = this.Height;
                }
                else
                {
                    rect.Width = image.Width;
                    rect.Height = image.Height;
                    Rectangle drect = new Rectangle(0, 0, this.Width, this.Height);
                    drect.Inflate(-this.Padding, -this.Padding);
                    Point pt = CalculateLeftEdgeTopEdge(this.ImageAlign, drect, image.Width, image.Height);
                    rect.Offset(pt);
                }
            }
            return (rect);
        }

        /// <summary>
        /// Gets the current image.
        /// </summary>
        /// <param name="btnState">State of the BTN.</param>
        /// <returns></returns>
        private Image GetCurrentImage(ButtonState btnState)
        {
            Image image = ImageIdle;
            switch (btnState)
            {
                case ButtonState.Idle:
                    if (this.Focused)
                    {
                        if (this.ImageFocused != null)
                            image = this.ImageFocused;
                    }
                    break;
                case ButtonState.MouseOver:
                    if (ImageMouseOver != null)
                        image = ImageMouseOver;
                    break;
                case ButtonState.Pushed:
                    if (ImagePressed != null)
                        image = ImagePressed;
                    break;
                case ButtonState.Disabled:
                    if (ImageInactive != null)
                    {
                        image = ImageInactive;
                    }
                    else
                    {
                        image = ImageIdle;
                    }

                    break;
            }
            return (image);
        }

        /// <summary>
        /// Calculates the left edge top edge.
        /// </summary>
        /// <param name="Alignment">The alignment.</param>
        /// <param name="rect">The rect.</param>
        /// <param name="nWidth">Width of the n.</param>
        /// <param name="nHeight">Height of the n.</param>
        /// <returns></returns>
        private Point CalculateLeftEdgeTopEdge(ContentAlignment Alignment, Rectangle rect, int nWidth, int nHeight)
        {
            Point pt = new Point(0, 0);
            switch (Alignment)
            {
                case ContentAlignment.BottomCenter:
                    pt.X = (rect.Width - nWidth) / 2;
                    pt.Y = rect.Height - nHeight;
                    break;
                case ContentAlignment.BottomLeft:
                    pt.X = 0;
                    pt.Y = rect.Height - nHeight;
                    break;
                case ContentAlignment.BottomRight:
                    pt.X = rect.Width - nWidth;
                    pt.Y = rect.Height - nHeight;
                    break;
                case ContentAlignment.MiddleCenter:
                    pt.X = (rect.Width - nWidth) / 2;
                    pt.Y = (rect.Height - nHeight) / 2;
                    break;
                case ContentAlignment.MiddleLeft:
                    pt.X = 0;
                    pt.Y = (rect.Height - nHeight) / 2;
                    break;
                case ContentAlignment.MiddleRight:
                    pt.X = rect.Width - nWidth;
                    pt.Y = (rect.Height - nHeight) / 2;
                    break;
                case ContentAlignment.TopCenter:
                    pt.X = (rect.Width - nWidth) / 2;
                    pt.Y = 0;
                    break;
                case ContentAlignment.TopLeft:
                    pt.X = 0;
                    pt.Y = 0;
                    break;
                case ContentAlignment.TopRight:
                    pt.X = rect.Width - nWidth;
                    pt.Y = 0;
                    break;
            }
            pt.X += rect.Left;
            pt.Y += rect.Top;
            return (pt);
        }


        /// <summary>
        /// Creates the region.
        /// </summary>
        /// <param name="nContract">The n contract.</param>
        private void CreateRegion(int nContract)
        {
            Point[] points = GetBorder(0, 0, this.Width, this.Height);
            BorderContract(nContract, ref points);
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.AddLines(points);
            this.Region = new Region(path);
        }

        /// <summary>
        /// Borders the contract.
        /// </summary>
        /// <param name="nPixel">The n pixel.</param>
        /// <param name="pts">The PTS.</param>
        private void BorderContract(int nPixel, ref Point[] pts)
        {
            int a = nPixel;
            pts[0].X += a; pts[0].Y += a;
            pts[1].X -= a; pts[1].Y += a;
            pts[2].X -= a; pts[2].Y += a;
            pts[3].X -= a; pts[3].Y += a;
            pts[4].X -= a; pts[4].Y -= a;
            pts[5].X -= a; pts[5].Y -= a;
            pts[6].X -= a; pts[6].Y -= a;
            pts[7].X += a; pts[7].Y -= a;
            pts[8].X += a; pts[8].Y -= a;
            pts[9].X += a; pts[9].Y -= a;
            pts[10].X += a; pts[10].Y += a;
            pts[11].X += a; pts[10].Y += a;
        }

        /// <summary>
        /// Gets the border.
        /// </summary>
        /// <param name="nLeftEdge">The n left edge.</param>
        /// <param name="nTopEdge">The n top edge.</param>
        /// <param name="nWidth">Width of the n.</param>
        /// <param name="nHeight">Height of the n.</param>
        /// <returns></returns>
        private Point[] GetBorder(int nLeftEdge, int nTopEdge, int nWidth, int nHeight)
        {
            int X = nWidth;
            int Y = nHeight;
            Point[] points = 
			{
				new Point(1   , 0  ),
				new Point(X-1 , 0  ),
				new Point(X-1 , 1  ),
				new Point(X   , 1  ),
				new Point(X   , Y-1),
				new Point(X-1 , Y-1),
				new Point(X-1 , Y  ),
				new Point(1   , Y  ),
				new Point(1   , Y-1),
				new Point(0   , Y-1),
				new Point(0   , 1  ),
				new Point(1   , 1  )
			};
            for (int i = 0; i < points.Length; i++)
            {
                points[i].Offset(nLeftEdge, nTopEdge);
            }
            return points;
        }

        /// <summary>
        /// Shades the specified s color.
        /// </summary>
        /// <param name="SColor">Color of the s.</param>
        /// <param name="RED">The red.</param>
        /// <param name="GREEN">The green.</param>
        /// <param name="BLUE">The blue.</param>
        /// <returns></returns>
        private static Color Shade(Color SColor, int RED, int GREEN, int BLUE)
        {
            int r = SColor.R;
            int g = SColor.G;
            int b = SColor.B;

            r += RED;
            if (r > 0xFF) r = 0xFF;
            if (r < 0) r = 0;

            g += GREEN;
            if (g > 0xFF) g = 0xFF;
            if (g < 0) g = 0;

            b += BLUE;
            if (b > 0xFF) b = 0xFF;
            if (b < 0) b = 0;

            return Color.FromArgb(r, g, b);
        }

        /// <summary>
        /// Blends the specified s color.
        /// </summary>
        /// <param name="SColor">Color of the s.</param>
        /// <param name="DColor">Color of the d.</param>
        /// <param name="Percentage">The percentage.</param>
        /// <returns></returns>
        private static Color Blend(Color SColor, Color DColor, int Percentage)
        {
            int r = SColor.R + ((DColor.R - SColor.R) * Percentage) / 100;
            int g = SColor.G + ((DColor.G - SColor.G) * Percentage) / 100;
            int b = SColor.B + ((DColor.B - SColor.B) * Percentage) / 100;
            return Color.FromArgb(r, g, b);
        }
        #endregion

        #region Events Methods

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseDown" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            this.Capture = true;
            this.capturingMouse = true;
            btnState = ButtonState.Pushed;
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseUp" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            btnState = ButtonState.Idle;
            this.Invalidate();
            this.capturingMouse = false;
            this.Capture = false;
            this.Invalidate();
        }

        /// <summary>
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if (!capturingMouse)
            {
                btnState = ButtonState.Idle;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseMove" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (capturingMouse)
            {
                Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
                btnState = ButtonState.Idle;
                if ((e.X >= rect.Left) && (e.X <= rect.Right))
                {
                    if ((e.Y >= rect.Top) && (e.Y <= rect.Bottom))
                    {
                        btnState = ButtonState.Pushed;
                    }
                }
                this.Capture = true;
                this.Invalidate();
            }
            else
            {
                //if(!this.Focused)
                {
                    if (btnState != ButtonState.MouseOver)
                    {
                        btnState = ButtonState.MouseOver;
                        this.Invalidate();
                    }
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            if (this.Enabled)
            {
                this.btnState = ButtonState.Idle;
            }
            else
            {
                this.btnState = ButtonState.Disabled;
            }
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnLostFocus(System.EventArgs)" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            if (this.Enabled)
            {
                this.btnState = ButtonState.Idle;
            }
            this.Invalidate();
        }


        #endregion
    }
}
